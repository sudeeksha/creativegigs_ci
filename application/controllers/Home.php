<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Function to show home page
     */
    public function index()
    {
        $data['pageName'] = "HOME";
        $this->load->view('content_handler', $data);
    }
    /**
     * Function to show about us page
     */
    public function about_us()
    {
        $data['pageName'] = "ABOUT US";
        $this->load->view('content_handler', $data);
    }
    /**
     * Function to show contact page
     */
    public function contact_us()
    {
        $data['pageName'] = "CONTACT US";
        $this->load->view('content_handler', $data);
    }

    /**
 * Function to show contact page
 */
    public function services()
    {
        $data['pageName'] = "SERVICES";
        $this->load->view('content_handler', $data);
    }

    /**
     * Function to show contact page
     */
    public function portfolio()
    {
        $data['pageName'] = "PORTFOLIO";
        $this->load->view('content_handler', $data);
    }

    /**
     * Function to show contact page
     */
    public function project_details()
    {
        $data['pageName'] = "PROJECT DETAILS";
        $this->load->view('content_handler', $data);
    }

    /**
     * Function to show contact page
     */
    public function team_details()
    {
        $data['pageName'] = "TEAM DETAILS";
        $this->load->view('content_handler', $data);
    }


    /**
     * Function to show contact page
     */
    public function service_details()
    {
        $data['pageName'] = "SERVICE DETAILS";
        $this->load->view('content_handler', $data);
    }
}
<style>
    .textcolor
    {
        color: white !important;
    }
</style>
    <!--
    =============================================
        Theme Inner Banner
    ==============================================
    -->
    <div class="theme-inner-banner section-spacing">
        <div class="overlay">
            <div class="container">
                <h2>Gallery</h2>
            </div> <!-- /.container -->
        </div> <!-- /.overlay -->
    </div> <!-- /.theme-inner-banner -->


    <!--
    =============================================
        Our Case
    ==============================================
    -->
    <div class="our-case our-project section-spacing">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block ">
                            <img src="<?php echo base_url('assets/images/portfolio/1.jpg');?>" alt="">
                            <div class="hover-content ">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/2.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/3.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>

                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/4.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/5.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/6.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/13.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5>Business Meeting</h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                    <a href="<?php echo site_url('home/project_details');?>" class="details float-right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/14.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5><a href="<?php echo site_url('home/project_details');?>">Business Meeting</a></h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-case-block">
                            <img src="<?php echo base_url('assets/images/portfolio/15.jpg');?>" alt="">
                            <div class="hover-content">
                                <div class="text clearfix textcolor">
                                    <div class="float-left">
                                        <h5><a href="<?php echo site_url('home/project_details');?>">Business Meeting</a></h5>
                                        <p>Explore strange new worlds</p>
                                    </div>
<!--                                    <a href="--><?php //echo site_url('home/project_details');?><!--" class="details float-right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>-->
                                </div> <!-- /.text -->
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.single-case-block -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.wrapper -->
<!--            <div class="theme-pagination text-center">-->
<!--                <ul>-->
<!--                    <li><a href="#">1</a></li>-->
<!--                    <li class="active"><a href="#">2</a></li>-->
<!--                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>-->
<!--                </ul>-->
<!--            </div>-->
        </div> <!-- /.container -->
    </div> <!-- /.our-case -->







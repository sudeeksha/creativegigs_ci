<?php

switch ($pageName) {
    case "HOME":
        include ("home.php");
        break;
    case "ABOUT US":
        include ("about_us.php");
        break;
    case "CONTACT US":
        include ("contact_us.php");
        break;
    case "SERVICES":
        include ("services.php");
        break;

    case "PORTFOLIO":
        include ("portfolio.php");
        break;

    case "PROJECT DETAILS":
        include ("project_details.php");
        break;

    case "TEAM DETAILS":
        include ("team_details.php");
        break;

    case "SERVICE DETAILS":
        include ("service_details.php");
        break;
}

?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.creativegigs.net/charles/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Jul 2018 10:34:23 GMT -->
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- For Window Tab Color -->
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#061948">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#061948">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#061948">
    <title>Korrency Consultants Pvt. Ltd.</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?php echo base_url('assets/images/fav-icon/icon.png'); ?>">
    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>">




    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900|Source+Sans+Pro:300,400,600,700,900">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/Camera-master/css/camera.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/menu/dist/css/slimmenu.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fonts/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/owl-carousel/owl.carousel.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/owl-carousel/owl.theme.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/WOW-master/css/libs/animate.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fonts/icon/font/flaticon.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fancybox/dist/jquery.fancybox.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/language-switcher/polyglot-language-switcher.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/sanzzy-map/dist/snazzy-info-window.min.css'); ?>">

    <script src="<?php echo base_url('assets/vendor/html5.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/html5shiv.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/respond.js'); ?>"></script>

</head>


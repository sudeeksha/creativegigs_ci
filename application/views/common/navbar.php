
<style>
     li.hovercolor :hover{
        color: #000f32 !important;
    }
    .listcolor{
        background-color: #000f32!important;
    }

     @media  (min-width: 320px) and (max-width: 480px) {

         .responseheight {
             height:15em;
         }
     }
</style>
<body>
<div class="main-page-wrapper">

    <!-- ===================================================
        Loading Transition
    ==================================================== -->
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>



    <!--
    =============================================
        Theme Header One
    ==============================================
    -->
    <header class="header-one">
        <div class="top-header">
            <div class="container clearfix">
                <div class="logo float-left"><a href="<?php echo site_url('home/index');?>">
                        <img src="<?php echo base_url('assets/images/logo/logo.png'); ?>" alt="">
                    </a></div>
                <div class="address-wrapper float-right">
                    <ul>
                        <li class="address">
                            <i class="icon flaticon-placeholder"></i>
                            <h6>Address:</h6>
                            <p>D-81 Shopping Complex, Opp. Dainik Bhaskar Press,<br/> A.B.

                                Road, Indore-452001 (MP)</p>
                        </li>
                        <li class="address">
                            <i class="icon flaticon-phone-call"></i>
                            <h6>Contact us:</h6>
                            <p>+91-93021 01258/ 78692 69899<br/>+91-731-4291258</p>
                        </li>

                    </ul>
                </div> <!-- /.address-wrapper -->
            </div> <!-- /.container -->
        </div> <!-- /.top-header -->

        <div class="theme-menu-wrapper">
            <div class="container">
                <div class="bg-wrapper clearfix">
                    <!-- ============== Menu Warpper ================ -->
                    <div class="menu-wrapper float-left">
                        <nav id="mega-menu-holder" class="clearfix responseheight">
                            <ul class="clearfix listcolor">
                                <li><a href="<?php echo site_url('home/index');?>">Home</a>
                                </li>
                                <li><a href="<?php echo site_url('home/about_us');?>">About</a>
                                </li>
                                <li><a href="<?php echo site_url('home/services');?>">Service</a>
                                    <ul class="dropdown">
                                        <li><a href="<?php echo site_url('home/services');?>">Loan Syndication (Project Financing)</a>
                                            <ul class="dropdown">
                                                <li><a href="<?php echo site_url('home/services');?>">Term Loan</a></li>
                                                <li><a href="<?php echo site_url('home/services');?>">Cash Credit Limit</a></li>
                                                <li><a href="<?php echo site_url('home/services');?>">Renewal of existing credit facilities</a></li>
                                            </ul></li>
                                        <li><a href="<?php echo site_url('home/services');?>">Home/ Mortgage Loan DSA for Nationalized and private banks</a></li>
                                        <li><a href="<?php echo site_url('home/services');?>">Liaison for various Central and/or State Sponsored subsidies</a></li>
                                        <li><a href="<?php echo site_url('home/services');?>">Management Consultancy &amp; Accounting including GST filing Services</a></li>
                                        <li><a href="<?php echo site_url('home/services');?>">Corporate Restructuring, Mergers &amp; Acquisitions, Valuation, Corporate Recovery</a></li>
                                        <li><a href="<?php echo site_url('home/services');?>">Business Advisory Services</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo site_url('home/portfolio');?>">Gallery</a>
                                </li>
                                <li><a href="<?php echo site_url('home/contact_us');?>">contact</a></li>
                                <li><a href="<?php echo site_url('home/team_details');?>">Our team</a></li>
                            </ul>
                        </nav> <!-- /#mega-menu-holder -->
                    </div> <!-- /.menu-wrapper -->

                    <div class="right-widget float-right">
                        <ul>
                            <li class="social-icon">
                                <ul>
                                    <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                </ul>
                            </li>
<!--                            <li class="cart-icon">-->
<!--                                <a href="#"><i class="flaticon-tool"></i> <span>2</span></a>-->
<!--                            </li>-->
<!--                            <li class="search-option">-->
<!--                                <div class="dropdown">-->
<!--                                    <button type="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search" aria-hidden="true"></i></button>-->
<!--                                    <form action="#" class="dropdown-menu">-->
<!--                                        <input type="text" Placeholder="Enter Your Search">-->
<!--                                        <button><i class="fa fa-search"></i></button>-->
<!--                                    </form>-->
<!--                                </div>-->
<!--                            </li>-->
                        </ul>
                    </div>


                    <!-- /.right-widget -->
                </div> <!-- /.bg-wrapper -->
            </div> <!-- /.container -->
        </div> <!-- /.theme-menu-wrapper -->
    </header> <!-- /.header-one -->
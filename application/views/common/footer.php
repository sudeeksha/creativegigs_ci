<style>
    .buttonsize
    {
        font-size: 30px;
        height:57px;
    }
</style>

<footer class="theme-footer-one">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-sm-6 about-widget">
                    <h6 class="title">About Us</h6>
                    <p>That started from this tropic port aboard this tiny ship today still want by theam government they survive on up to thetre east side to a deluxe as soldiers of artics fortune.</p>

                </div> <!-- /.about-widget -->



                <div class="col-xl-4 col-lg-4 col-sm-4 about-widget footer-list">
                    <h6 class="title">Services</h6>
                        <ul>
                            <li><a href="<?php echo site_url('home/services');?>">Loan Syndication (Project Financing)</a></li>
                            <li><a href="<?php echo site_url('home/services');?>">Home/ Mortgage Loan DSA for Nationalized and private banks</a></li>
                            <li><a href="<?php echo site_url('home/services');?>">Liaison for various Central and/or State Sponsored subsidies</a></li>
                            <li><a href="<?php echo site_url('home/services');?>">Management Consultancy &amp; Accounting including GST filing Services</a></li>
                            <li><a href="<?php echo site_url('home/services');?>">Corporate Restructuring, Mergers &amp; Acquisitions, Valuation, Corporate Recovery</a></li>
                            <li><a href="<?php echo site_url('home/services');?>">Business Advisory Services</a></li>
                        </ul>

                </div>
                <div class="col-xl-4 col-lg-4 col-sm-4 about-widget queries">

                   <h6 class="title">Address</h6>
                    <div class="queries">
                        <i class="icon flaticon-placeholder"></i>Address : <p>D-81 Shopping Complex, Opp. Dainik Bhaskar Press,<br/> A.B.
                            Road, Indore-452001 (MP)</p>
                    </div>

                    <div class="queries">
                        <i class="flaticon-phone-call"></i> Any Queries : <a href="tel:78692 69899"  data-action="call" class="moto-link">+91-93021 01258/ 78692 69899<br/>

                            +91-731-4291258</a>
                    </div>

                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.top-footer -->
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12"><p>&copy; Copyright © 2018 Korrency Consultants Pvt. Ltd All Rights Reserved.</p></div>
                <div class="col-md-6 col-12">
                    <ul>
                        <li><a href="<?php echo site_url('home/about_us');?>">About</a></li>
                        <li><a href="<?php echo site_url('home/services');?>">Services</a></li>
                        <li><a href="<?php echo site_url('home/portfolio');?>">Gallery</a></li>

                        <li><a href="<?php echo site_url('home/contact_us');?>">Contact</a></li>
                        <li><a href="<?php echo site_url('home/team_details');?>">Our team</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- /.bottom-footer -->
</footer> <!-- /.theme-footer -->




<!-- Scroll Top Button -->
<button class="scroll-top tran3s buttonsize">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</button>



<!-- Optional JavaScript _____________________________  -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- jQuery -->
<script src="<?php echo base_url('assets/vendor/jquery.2.2.3.min.js'); ?>"></script>
<!-- Popper js -->
<script src="<?php echo base_url('assets/vendor/popper.js/popper.min.js'); ?>"></script>
<!-- Bootstrap JS -->
<script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
<!-- Camera Slider -->
<script src='<?php echo base_url('assets/vendor/Camera-master/scripts/jquery.mobile.customized.min.js'); ?>'></script>
<script src='<?php echo base_url('assets/vendor/Camera-master/scripts/jquery.easing.1.3.js'); ?>'></script>
<script src='<?php echo base_url('assets/vendor/Camera-master/scripts/camera.min.js'); ?>'></script>
<!-- menu  -->
<script src="<?php echo base_url('assets/vendor/menu/src/js/jquery.slimmenu.js'); ?>"></script>
<!-- WOW js -->
<script src="<?php echo base_url('assets/vendor/WOW-master/dist/wow.min.js'); ?>"></script>
<!-- owl.carousel -->
<script src="<?php echo base_url('assets/vendor/owl-carousel/owl.carousel.min.js'); ?>"></script>
<!-- js count to -->
<script src="<?php echo base_url('assets/vendor/jquery.appear.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery.countTo.js'); ?>"></script>
<!-- Fancybox -->
<script src="<?php echo base_url('assets/vendor/fancybox/dist/jquery.fancybox.min.js'); ?>"></script>

<!-- Theme js -->
<script src="<?php echo base_url('assets/js/theme.js'); ?>"></script>
</div> <!-- /.main-page-wrapper -->
</body>

<!-- Mirrored from html.creativegigs.net/charles/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Jul 2018 10:35:47 GMT -->
</html>
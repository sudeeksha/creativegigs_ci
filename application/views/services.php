
    <!--
    =============================================
        Theme Inner Banner
    ==============================================
    -->
    <div class="theme-inner-banner section-spacing">
        <div class="overlay">
            <div class="container">
                <h2>SERVICES</h2>
            </div> <!-- /.container -->
        </div> <!-- /.overlay -->
    </div> <!-- /.theme-inner-banner -->


    <!--
    =============================================
        Our Solution
    ==============================================
    -->
    <div class="our-solution section-spacing">
        <div class="container">
            <div class="theme-title-one">
                <h2>Our SERVICES</h2>
            </div> <!-- /.theme-title-one -->
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-solution-block">
                            <img src="<?php echo base_url('assets/images/icon/5.png');?>" alt="" class="icon">
                            <h5><a href="<?php echo site_url('home/service_details');?>">Loan Syndication (Project Financing)</a></h5>
                            <p>The explore strange new worlds to seek fout new life and new civilizations to boldly where no man has before gone. </p>
                        </div> <!-- /.single-solution-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-solution-block">
                            <img src="<?php echo base_url('assets/images/icon/6.png');?>" alt="" class="icon">
                            <h5><a href="<?php echo site_url('home/service_details');?>">Management Consultancy &amp; Accounting including GST filing Services</a></h5>
                            <p>The explore strange new worlds to seek fout new life and new civilizations to boldly where no man has before gone. </p>
                        </div> <!-- /.single-solution-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-solution-block">
                            <img src="<?php echo base_url('assets/images/icon/7.png');?>" alt="" class="icon">
                            <h5><a href="<?php echo site_url('home/service_details');?>">Corporate Restructuring, Mergers &amp; Acquisitions, Valuation, Corporate Recovery</a></h5>
                            <p>The explore strange new worlds to seek fout new life and new civilizations to boldly where no man has before gone. </p>
                        </div> <!-- /.single-solution-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-solution-block">
                            <img src="<?php echo base_url('assets/images/icon/8.png');?>" alt="" class="icon">
                            <h5><a href="<?php echo site_url('home/service_details');?>">Business Advisory Services</a></h5>
                            <p>The explore strange new worlds to seek fout new life and new civilizations to boldly where no man has before gone. </p>
                        </div> <!-- /.single-solution-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-solution-block">
                            <img src="<?php echo base_url('assets/images/icon/9.png');?>" alt="" class="icon">
                            <h5><a href="<?php echo site_url('home/service_details');?>">Home/ Mortgage Loan DSA for Nationalized and private banks</a></h5>
                            <p>The explore strange new worlds to seek fout new life and new civilizations to boldly where no man has before gone. </p>
                        </div> <!-- /.single-solution-block -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="single-solution-block">
                            <img src="<?php echo base_url('assets/images/icon/10.png');?>" alt="" class="icon">
                            <h5><a href="<?php echo site_url('home/service_details');?>">Liaison for various Central and/or State Sponsored subsidies</a></h5>
                            <p>The explore strange new worlds to seek fout new life and new civilizations to boldly where no man has before gone. </p>
                        </div> <!-- /.single-solution-block -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.our-solution -->



    <!--
    =============================================
        Feature Banner
    ==============================================
    -->
    <div class="feature-banner section-spacing">
        <div class="opacity">
            <div class="container">
                <h2>We provide high quality services &amp; innovative solutions for the realiable growth</h2>
                <a href="#" class="theme-button-one">GET A QUOTES</a>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div> <!-- /.feature-banner -->









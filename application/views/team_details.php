<style>
    .aligntext{
       text-align: justify;
        font-size: 13px;
        margin: 0px 7px 0px 7px;
    }
    .textcolsize{
        height:19em;

    }
</style>
    <div class="theme-inner-banner section-spacing">
        <div class="overlay">
            <div class="container">
                <h2>Our Expert</h2>
            </div> <!-- /.container -->
        </div> <!-- /.overlay -->
    </div> <!-- /.theme-inner-banner -->


    <!--
    =====================================================
        Our Team
    =====================================================
    -->
    <div class="our-team section-spacing">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="team-member">
                            <div class="image-box">
                                <img src="<?php echo base_url('assets/images/team/1.jpg');?>" alt="">
                                <div class="overlay">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        </ul>
                                        <p>CA.Chain Sukh Mandowara</p>
                                        <p>Director</p>
                                    </div> <!-- /.hover-content -->
                                </div> <!-- /.overlay -->
                            </div> <!-- /.image-box -->
                            <div class="text textcolsize">
                                <h6>CA.Chain Sukh Mandowara</h6>
                                <span>Director</span>
                                <p class="aligntext">CA.Chain Sukh Mandowara is commerce Graduate and passed out Chartered Accountancy
                                    course in 1994. He is having a vast experience of over 24 years in field of Project Financing
                                    viz Term Loan, Working Capital and Trade Finance for SME, tie up for various subsidy such
                                    as Industrial Development Policy of MP State, PMEGP, Mukhya Mantri Yuva Udhyami Scheme
                                    &amp; Mukhymantri Yuva Swarojgar Scheme, Warehouse, Cold Storages, Food Processing
                                    industries, NABARD etc.</p>
                            </div>

                           <!-- /.text -->

                        </div><!-- /.team-member -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="team-member">
                            <div class="image-box">
                                <img src="<?php echo base_url('assets/images/team/2.jpg');?>" alt="">
                                <div class="overlay">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        </ul>
                                        <p>Mr. Govind Dosee:</p>
                                        <p>Founder, CEO</p>
                                    </div> <!-- /.hover-content -->
                                </div> <!-- /.overlay -->
                            </div> <!-- /.image-box -->
                            <div class="text textcolsize">
                                <h6>Mr. Govind Dosee:</h6>
                                <span>Founder, CEO</span>
                                <p class="aligntext">He has passed out Post Graduation Course in Accounts &amp; Taxation with D.A.V.V. in 1995 and
                                    having well working experience of more than 22 Years in well known Pharmaceuticals company
                                    of Indore and heading Accounts and Finance Department including overseas operations. He is
                                    looking after various subsidy matters under Industrial Development Policy of Govt. of MP like
                                    Capital Subsidy, Interest Subsidy and VAT Subsidy.</p>

                            </div>

                            <!-- /.text -->
                        </div> <!-- /.team-member -->
                    </div> <!-- /.col- -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="team-member">
                            <div class="image-box">
                                <img src="<?php echo base_url('assets/images/team/4.jpg');?>" alt="">
                                <div class="overlay">
                                    <div class="hover-content">
                                        <ul>
                                            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        </ul>
                                        <p>Vijay Koshti</p>
                                        <p>Marketing Lead</p>
                                    </div> <!-- /.hover-content -->
                                </div> <!-- /.overlay -->
                            </div> <!-- /.image-box -->
                            <div class="text textcolsize">
                                <h6>Vijay Koshti</h6>
                                <span>Marketing Lead</span>
                                <p class="aligntext"> Is Commerce graduate having over six years experience in Govt. licensing registration under
                                    various laws viz GST, Udhyog Adhar, Gumasta (Shop &amp; Establishment Act), Nagar Nigan
                                    License etc.</p>
                            </div>


                            <!-- /.text -->
                        </div> <!-- /.team-member -->
                    </div> <!-- /.col- -->

                </div> <!-- /.row -->
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.our-team -->









<style>
    @media  (min-width: 320px) and (max-width: 480px) {

        .mediaresponsive {
            margin-left: 0% !important;
            margin-top: -33% !important;
        }
    }
    @media  (min-width: 320px) and (max-width: 480px) {

        .mediasize {
            margin-top: 10%;
        }
    }
    .aboutussize{
        margin-left: 48%;
        margin-top: -35%;
    }
.imagesize{
    padding-top: 71px;
}
    .textsize{
        height: 14em;
    }
    
</style>
<div id="theme-main-banner" class="banner-one">
    <div data-src="<?php echo base_url('assets/images/home/slide-1.jpg');?>">
        <div class="camera_caption">
            <div class="container">
                <p class="wow fadeInUp animated">The government they survive artical of fortune</p>
                <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">We IMPROVE YOUR <br>SALES EFFICIENCY</h1>
                <a href="<?php echo site_url('home/contact_us');?>" class="theme-button-one wow fadeInUp animated" data-wow-delay="0.39s">CONTACT US</a>
            </div> <!-- /.container -->
        </div> <!-- /.camera_caption -->
    </div>
    <div data-src="<?php echo base_url('assets/images/home/slide-2.jpg');?>">
        <div class="camera_caption">
            <div class="container">
                <p class="wow fadeInUp animated">The government they survive artical of fortune</p>
                <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">We IMPROVE YOUR <br>SALES EFFICIENCY</h1>
                <a href="<?php echo site_url('home/contact_us');?>" class="theme-button-one wow fadeInUp animated" data-wow-delay="0.39s">CONTACT US</a>
            </div> <!-- /.container -->
        </div> <!-- /.camera_caption -->
    </div>
    <div data-src="<?php echo base_url('assets/images/home/slide-3.jpg');?>">
        <div class="camera_caption">
            <div class="container">
                <p class="wow fadeInUp animated">The government they survive artical of fortune</p>
                <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">We IMPROVE YOUR <br>SALES EFFICIENCY</h1>
                <a href="<?php echo site_url('home/contact_us');?>" class="theme-button-one wow fadeInUp animated" data-wow-delay="0.39s">CONTACT US</a>
            </div> <!-- /.container -->
        </div> <!-- /.camera_caption -->
    </div>
</div> <!-- /#theme-main-banner -->


<!--
=============================================
    About Company
==============================================
-->
<div class="about-compnay section-spacing">
    <div class="container">
        <div class="row1 mediasize">
            <div class="col-lg-6 col-12"><img class="imagesize" src="<?php echo base_url('assets/images/home/1.jpg');?>" alt=""></div>
            <div class="col-lg-6 col-12 mediaresponsive aboutussize">
                <div class="text">
                    <div class="theme-title-one">
                        <h2>About Our Company</h2>
                        <p>A tale of a fateful trip that started from this tropic port aboard this tiny ship today still wanted by the government they survive as soldiers of fortune to a deluxe apartment in the sky moving on up to the east side a family.</p>
                        <p>The government they survive as soldiers of fortune baby if you've ever wondered the east side to a deluxe apartment.</p>
                    </div> <!-- /.theme-title-one -->
                    <ul class="mission-goal clearfix">
                        <li>
                            <i class="icon flaticon-star"></i>
                            <h4>Vision</h4>
                        </li>
                        <li>
                            <i class="icon flaticon-medal"></i>
                            <h4>Missions</h4>
                        </li>
                        <li>
                            <i class="icon flaticon-target"></i>
                            <h4>Goals</h4>
                        </li>
                    </ul> <!-- /.mission-goal -->
                </div> <!-- /.text -->
            </div> <!-- /.col- -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div>




<!--
=============================================
    Service Style One
==============================================
-->
<div class="service-style-one section-spacing">
    <div class="container">
        <div class="theme-title-one">
            <h2>Our SERVICES</h2>
            <p>A tale of a fateful trip that started from this tropic port aboard this tiny ship today stillers</p>
        </div> <!-- /.theme-title-one -->
        <div class="wrapper">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="single-service">
                        <div class="img-box"><img src="<?php echo base_url('assets/images/home/3.jpg'); ?>" alt=""></div>
                        <div class="text textsize">
                            <h5><a href="<?php echo site_url('home/services');?>">Loan Syndication (Project Financing)</a></h5>
                            <p>The tiny ship today stiller</p>
                            <a href="<?php echo site_url('home/services');?>" class="read-more">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="single-service">
                        <div class="img-box"><img src="<?php echo base_url('assets/images/home/4.jpg'); ?>" alt=""></div>
                        <div class="text textsize">
                            <h5><a href="<?php echo site_url('home/services');?>">Management Consultancy &amp; Accounting including GST filing Services</a></h5>
                            <p>The tiny ship today stiller</p>
                            <a href="<?php echo site_url('home/services');?>" class="read-more">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="single-service">
                        <div class="img-box"><img src="<?php echo base_url('assets/images/home/5.jpg'); ?>" alt=""></div>
                        <div class="text">
                            <h5><a href="<?php echo site_url('home/services');?>">Corporate Restructuring, Mergers &amp; Acquisitions, Valuation, Corporate Recovery</a></h5>
                            <p>The tiny ship today stiller</p>
                            <a href="<?php echo site_url('home/services');?>" class="read-more">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="single-service">
                        <div class="img-box"><img src="<?php echo base_url('assets/images/home/6.jpg'); ?>" alt=""></div>
                        <div class="text textsize">
                            <h5><a href="<?php echo site_url('home/services');?>">Business Advisory Services</a></h5>
                            <p>The tiny ship today stiller</p>
                            <a href="<?php echo site_url('home/services');?>" class="read-more">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="single-service">
                        <div class="img-box"><img src="<?php echo base_url('assets/images/home/7.jpg'); ?>" alt=""></div>
                        <div class="text textsize">
                            <h5><a href="<?php echo site_url('home/services');?>">Home/ Mortgage Loan DSA for Nationalized and private banks</a></h5>
                            <p>The tiny ship today stiller</p>
                            <a href="<?php echo site_url('home/services');?>" class="read-more">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="single-service">
                        <div class="img-box"><img src="<?php echo base_url('assets/images/home/8.jpg'); ?>" alt=""></div>
                        <div class="text textsize">
                            <h5><a href="<?php echo site_url('home/services');?>">Liaison for various Central and/or State Sponsored subsidies</a></h5>
                            <p>The tiny ship today stiller</p>
                            <a href="<?php echo site_url('home/services');?>" class="read-more">READ MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.wrapper -->
        <div class="contact-text">
            <h4>You can call us</h4>
            <h5><a href="tel:+91-731-4291258"  data-action="call">+91-731-4291258</a></h5>
        </div>
    </div> <!-- /.container -->
</div> <!-- /.service-style-one -->


<!--
=====================================================
    Testimonial Slider
=====================================================
-->
<div class="testimonial-section section-spacing">
    <div class="overlay">
        <div class="container">
            <div class="wrapper">
                <div class="bg">
                    <div class="testimonial-slider">
                        <div class="item">
                            <p>“ A tale of a fateful trip that started from this tropic port aboard this tiny ship today still wanted by the government they survive as soldiers
                                of fortune to a deluxe apartment in the sky moving on up to the east side a family. ”</p>
                            <div class="name">
                                <h6>CA. Chain Sukh Mandowara</h6>
                                <span>Director</span>
                            </div> <!-- /.name -->
                        </div> <!-- /.item -->
                        <div class="item">
                            <p>“ A tale of a fateful trip that started from this tropic port aboard this tiny ship today still wanted by the government they survive as soldiers of fortune to a deluxe apartment in the sky moving on up to the east side a family. ”</p>
                            <div class="name">
                                <h6>Mr. Govind Dosee</h6>
                                <span>Director</span>
                            </div> <!-- /.name -->
                        </div> <!-- /.item -->
                        <div class="item">
                            <p>“ A tale of a fateful trip that started from this tropic port aboard this tiny ship today still wanted by the government they survive as soldiers of fortune to a deluxe apartment in the sky moving on up to the east side a family. ”</p>
                            <div class="name">
                                <h6>Vijay Koshti</h6>
                                <span>Director</span>
                            </div> <!-- /.name -->
                        </div> <!-- /.item -->
                    </div> <!-- /.testimonial-slider -->
                </div> <!-- /.bg -->
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.overlay -->
</div> <!-- /.testimonial-section -->


<!--
=====================================================
    Our Team
=====================================================
-->
<div class="our-team section-spacing">
    <div class="container">
        <div class="theme-title-one">
            <h2>Our TEAM</h2>
            <p>A tale of a fateful trip that started from this tropic port aboard this tiny ship today stillers</p>
        </div> <!-- /.theme-title-one -->
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="team-member">
                        <div class="image-box">
                            <img src="<?php echo base_url('assets/images/team/1.jpg'); ?>" alt="">
                            <div class="overlay">
                                <div class="hover-content">
                                    <ul>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    </ul>
                                    <p>CA. Chain Sukh Mandowara</p>
                                    <p>Director</p>
                                </div> <!-- /.hover-content -->
                            </div> <!-- /.overlay -->
                        </div> <!-- /.image-box -->
                        <div class="text">
                            <h6>CA. Chain Sukh Mandowara</h6>
                            <span>Director</span>
                        </div> <!-- /.text -->
                    </div> <!-- /.team-member -->
                </div> <!-- /.col- -->
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="team-member">
                        <div class="image-box">
                            <img src="<?php echo base_url('assets/images/team/2.jpg'); ?>" alt="">
                            <div class="overlay">
                                <div class="hover-content">
                                    <ul>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    </ul>
                                    <p>Mr. Govind Dosee:</p>
                                    <p>Founder, CEO</p>
                                </div> <!-- /.hover-content -->
                            </div> <!-- /.overlay -->
                        </div> <!-- /.image-box -->
                        <div class="text">
                            <h6>Mr. Govind Dosee:</h6>
                            <span>Founder, CEO</span>
                        </div> <!-- /.text -->
                    </div> <!-- /.team-member -->
                </div> <!-- /.col- -->
               <!-- /.col- -->
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="team-member">
                        <div class="image-box">
                            <img src="<?php echo base_url('assets/images/team/4.jpg'); ?>" alt="">
                            <div class="overlay">
                                <div class="hover-content">
                                    <ul>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.linkedin.com/start/join?_l=en"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="https://in.pinterest.com/"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    </ul>
                                    <p>Vijay Koshti</p>
                                    <p></p>
                                </div> <!-- /.hover-content -->
                            </div> <!-- /.overlay -->
                        </div> <!-- /.image-box -->
                        <div class="text">
                            <h6>Vijay Koshti</h6>
                            <span>SEO Analyst</span>
                        </div> <!-- /.text -->
                    </div> <!-- /.team-member -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.wrapper -->
    </div> <!-- /.container -->
</div> <!-- /.our-team -->


<!--
=====================================================
    Theme Counter
=====================================================
-->
<div class="theme-counter section-spacing">
    <div class="container">
        <div class="bg">
            <h6>Company Success</h6>
            <h2>Some fun facts about our consulting</h2>

            <div class="cunter-wrapper">
                <div class="row">
                    <div class="col-md-3 col-6">
                        <div class="single-counter-box">
                            <div class="number"><span class="timer" data-from="0" data-to="30" data-speed="1200" data-refresh-interval="5">0</span>+</div>
                            <p>Years of Excellence</p>
                        </div> <!-- /.single-counter-box -->
                    </div>  <!-- /.col- -->
                    <div class="col-md-3 col-6">
                        <div class="single-counter-box">
                            <div class="number"><span class="timer" data-from="0" data-to="100" data-speed="1200" data-refresh-interval="5">0</span>%</div>
                            <p>Client Satisfaction</p>
                        </div> <!-- /.single-counter-box -->
                    </div>  <!-- /.col- -->
                    <div class="col-md-3 col-6">
                        <div class="single-counter-box">
                            <div class="number"><span class="timer" data-from="0" data-to="53" data-speed="1200" data-refresh-interval="5">0</span>k</div>
                            <p>Cases Completed</p>
                        </div> <!-- /.single-counter-box -->
                    </div>  <!-- /.col- -->
                    <div class="col-md-3 col-6">
                        <div class="single-counter-box">
                            <div class="number"><span class="timer" data-from="0" data-to="24" data-speed="1200" data-refresh-interval="5">0</span></div>
                            <p>Consultants</p>
                        </div> <!-- /.single-counter-box -->
                    </div>  <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.cunter-wrapper -->
            <a href="#" class="theme-button-one">VIEW CASE STUDIES</a>
        </div> <!-- /.bg -->
    </div> <!-- /.container -->
</div> <!-- /.theme-counter -->


<!--
=====================================================
    Free Consultation
=====================================================
-->
<div class="consultation-form section-spacing">
    <div class="container">
        <div class="theme-title-one">
            <h2>FREE CONSULTATION</h2>
            <p>A tale of a fateful trip that started from this tropic port aboard this tiny ship today stillers</p>
        </div> <!-- /.theme-title-one -->
        <div class="clearfix main-content no-gutters row">
            <div class="col-xl-6 col-lg-5 col-12"><div class="img-box"></div></div>
            <div class="col-xl-6 col-lg-7 col-12">
                <div class="form-wrapper">
                    <form action="#" class="theme-form-one">
                        <div class="row">
                            <div class="col-md-6"><input type="text" placeholder="Name *"></div>
                            <div class="col-md-6"><input type="text" placeholder="Phone *"></div>
                            <div class="col-md-6"><input type="email" placeholder="Email *"></div>
                            <div class="col-md-6">
                                <select class="form-control" id="exampleSelect1">
                                    <option>Service you’re looking for?</option>
                                    <option>Business Services</option>
                                    <option>Consumer Product</option>
                                    <option>Financial Services</option>
                                    <option>Software Research</option>
                                </select>
                            </div>
                            <div class="col-12"><textarea placeholder="Message"></textarea></div>
                        </div> <!-- /.row -->
                        <button class="theme-button-one">GET A QUOTES</button>
                    </form>
                </div> <!-- /.form-wrapper -->
            </div> <!-- /.col- -->
        </div> <!-- /.main-content -->
    </div> <!-- /.container -->
</div> <!-- /.consultation-form -->



<!--
=====================================================
    Partner Slider
=====================================================
-->
<!-- /.partner-section -->